#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <errno.h>
#define MAX_ARGUMENTS 200
void err_exit(char *error_message) {
	//char error_message[30] = "An error has occurred\n";
	write(STDERR_FILENO,error_message, strlen(error_message));
}


void execute(char *[], char *[]);


/* putsaa rivin turhilta merkeiltä ja luo rivistä taulukon.*/
void parseLine(char *line, char **argv) {
	char separator[] = " ";
	char *buf;
	int i= 0;
	char *pos;
	while ((pos = strchr(line, '\t')) != NULL) {
		*pos = ' ';
	}
	argv[0] = strtok(line, separator);
	while ((buf = strtok(NULL, separator))!= NULL) {
		if(i == MAX_ARGUMENTS) {
			err_exit("Inputline was too long, exiting from program.\n");
			exit(1);
		}
		++i;
		argv[i] = buf;
	}
	argv[i][strcspn(argv[i], "\n")] = 0;
	if (strlen(argv[i]) == 0)
		argv[i] = NULL;
	else
		argv[i +1] = NULL;
}	

/* pilkkoo taulukon osiin &-merkin kohdalta. commands[]-taulukkoon tulee
 * aina seuraavan komennon kohta argv-taulukosta. Palauttaa komentojen määrän*/
int get_commands_from_line(char *argv[], int commands[]) {
	int i, j=1;

	commands[0] = 0;
	for (i = 1; argv[i] != NULL; ++i) {
		if (strcmp(argv[i], "&") == 0) {
			argv[i] = NULL;
			if(argv[i+1] != NULL) {
				commands[j] = i+ 1;
				j++;
			}
		}
		
	}
	commands[j] = 999;
	return j;
}


/* Tsekkaa, onko komennossa >-merkki. jos on, niin palauttaa sen jälkeisen
 * alkion merkkijonona, joka toimii tiedostonimenä.*/
char* check_redirect(char *argv[]) {

	for (int i = 0; argv[i] != NULL; ++i) {
		if (strcmp(argv[i], ">")==0){
			if (argv[i+2] == NULL && argv[i+1] != NULL) {
				argv[i] = NULL;
				return argv[i+1];
			}
			else {
				err_exit("redirection error: you can redirect only to one file.\n");
				exit(1);
			}
		}
	}
	return NULL;
}


/* Etsitään poluilta komento.*/
int find_command_from_paths(char*paths[], char *arg[],char path[]) {
	char buffer[CHAR_MAX];
	for (int i = 0; paths[i] !=  NULL; ++i) {
		strcpy(buffer, paths[i]);
		if( paths[i][-1] != '/')
			strcat(buffer, "/");
		strcat(buffer, arg[0]);
		if(access(buffer, X_OK) == 0){
			strcpy(path, buffer);
			return 0;
		}
	}
	return -1;
}


/* asettaa uudet polut, joista komentoja etsitään.*/
void new_paths(char *argv [], char *paths[]) {
	paths[0] = NULL;
	char x;
	int i;
	for (i = 0; argv[i] != NULL; ++i) {
		paths[i] = realloc(paths[i], (strlen(argv[i]) * sizeof x)) ;
		strcpy(paths[i], argv[i]);
	}
	paths[i+1] = NULL;
}


/* tsekkaa, onko komento built-in. Jos on, suorittaa sen ja palauttaa ykkösen,
 * muuten 0.*/
int run_built_ins(char *arguments[], char *paths[]) {
	if(strcmp(arguments[0],"exit")==0) {
		if (arguments[1]) {
			err_exit("Exit takes no arguments.\n");
			return 1;
		}else 
			exit(0);
	}
	else if(strcmp(arguments[0], "path") == 0) {
		new_paths(arguments+1, paths);
		return 1;
	}
	else if (strcmp(arguments[0], "cd") == 0) {
		if (arguments[1] != NULL && arguments[2] == NULL) {
			if (chdir(arguments[1]) == -1) {
				err_exit("Error in direction change. Give working direction.\n");	
			}
		}
		else {
			err_exit("cd usage: cd [direction]\n");
		}
		return 1;
	}
	return 0;
}


/* ohjaa from fileno to out */
void create_dup(char *out, int fileno) {
	int fd;
	if ((fd = creat(out, 0644)) != -1) {
		if(dup2(fd, fileno)== -1) {
			err_exit("dup2 error.\n");
			exit(1);
		}
		close(fd);
		} else {
			err_exit("creat error: couldn't open file for redirection.\n");
			exit(1);
		}
}


/* Suorittaa rivin. Switch-rakenne otettu viikkotehtävästä 10-3.*/
void execute(char *argv[], char *paths[]) {
	pid_t pids[MAX_ARGUMENTS];
	int status;
	int i;
	int commands[MAX_ARGUMENTS];
	int n = get_commands_from_line(argv, commands);
	char* out;
	char path [CHAR_MAX] = "";

	
	for (i = 0; i < n; ++i) {
		if ((argv + commands[i])== NULL) continue;
		if(run_built_ins(argv+commands[i], paths) == 1) {
			continue;
		}
		if(find_command_from_paths(paths, argv+commands[i], path) == -1) {
			err_exit("No such command in given paths.\n");
			continue;
			}		
		out = check_redirect(argv+commands[i]);

		switch (pids[i] = fork()) {
			case -1:              /* error in fork */
				err_exit("fork error.\n");
				exit(1);
			case 0:               /* child process */
				if (out) {
					create_dup(out, STDOUT_FILENO);				
				}
				if (execvp(path, argv+commands[i]) == -1) {
					if (out) {
						create_dup(out, STDERR_FILENO);	
					}
					perror("execvp");
					exit(1);
				}
				exit(0);
				break;
		}
	}
	while(wait(&status) >0){} 
}


/* asettaa luettavan tiedoston.*/
void get_file(int argc, char *argv[], FILE **filu) {
	if (argc==2) {
		if ((*filu = fopen(argv[1], "r")) == NULL) {
			err_exit("Couldn't open given batch-file.\n");
			exit(1);
		}
	}
	else if (argc == 1) {
		*filu = stdin;
	}
	else {
		err_exit("Give only one batch-file.\n");
		exit(0);
	}
}


int main(int argc, char *argv[])
{
	char *arguments[MAX_ARGUMENTS];
	size_t size;
	char *buffer = NULL;
	FILE *filu=NULL;
	char *paths[MAX_ARGUMENTS] = {"/bin", "/usr/bin", "\0"};
	get_file(argc, argv, &filu);
	int read;
	if (filu == stdin)
		printf("wish> ");
	while ((read = getline(&buffer, &size, filu)) != -1 ) {
		
		parseLine(buffer, arguments);	
		if (arguments[0] == NULL) {
			if (filu == stdin) 
				printf("wish> ");
			continue;
		}
		execute(arguments, paths);
		if (filu == stdin) {
			printf("wish> ");
		}
	}
	fclose(filu);	
	return 0;
}
