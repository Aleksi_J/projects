This project is a very simple shell written in C.
To install this run "make" command.

Here's a list of things what this shell can do:

1. Redirection
  * with >-character it is possible to redirect the standard input.
  * for example: "ls -la /tmp > output" will direct stdin to "output" named file.
2. Parallel commands
  * with "&"-character it is possible to run different commands parallel.
3. built-in commands:
    1. exit
        * close shell.
    2. cd
        * moves to given directory.
    3. path
        * specify directories where to search commands.
	    * for example "path /bin" will set path to bin-directory.
4. batch mode
  * read input from file and executes commands from there.
  * for example "./wish batch.txt"


