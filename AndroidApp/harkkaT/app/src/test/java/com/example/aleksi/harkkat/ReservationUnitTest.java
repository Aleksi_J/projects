package com.example.aleksi.harkkat;

import com.example.aleksi.harkkat.exceptions.DuplicateException;
import com.example.aleksi.harkkat.reservation.Reservation;
import com.example.aleksi.harkkat.reservation.RoomReservationController;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class ReservationUnitTest {

    @Test
    public void testParticipantAddingAndRemoving() {
        Calendar cc = Calendar.getInstance();
        cc.set(2000, 11, 30, 14, 20);
        Date start = cc.getTime();

        cc.set(2000,11,30,16,20);
        Date end = cc.getTime();
        Reservation res = new Reservation(20, "turnaus", 12, start, end);
        int i;
        try {
            for (i = 0; i < 20; i++) {
                res.addParticipant(i + 10);
            }
        }catch (IndexOutOfBoundsException e) {
            assertTrue(false);
        } catch (DuplicateException e) {
            e.printStackTrace();
        }

        try {
            res.addParticipant(50);
            assertTrue(false);
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        } catch (DuplicateException e) {
            assertTrue(false);
        }

        assertEquals(0,res.getEventID());


        boolean val = res.removeParticipant(11);
        boolean ans = true;
        assertEquals(ans, val);

        val = res.removeParticipant(1);
        ans = false;
        assertEquals(ans, val);


    }

    @Test
    public void testReservationAdding() {

        Date start = new Date(2000,11,30,14,20);
        Date end = new Date(2000,11,30,16,20);

        Reservation res = new Reservation(20, "turnaus", 12, start, end);

        RoomReservationController rrc = new RoomReservationController("testihuone", "testipaikka", false, 10, 40);

        boolean value = rrc.addReservation(res);
        boolean answer = true;
        assertEquals(answer,value);
        start = new Date(2000,11,30,17,21);
        end = new Date(2000,11,30,17,30);

        res = new Reservation(20, "turnaus", 12, start, end);
        value = rrc.addReservation(res);
        answer = true;
        assertEquals(answer,value);


        start = new Date(2000,11,30,16,19);
        end = new Date(2000,11,30,16,40);

        res = new Reservation(20, "turnaus", 12, start, end);
        value = rrc.addReservation(res);
        answer = false;
        assertEquals(answer,value);


        start = new Date(2000,11,30,16,30);
        end = new Date(2000,11,30,16,40);

        res = new Reservation(20, "turnaus", 12, start, end);
        value = rrc.addReservation(res);
        answer = true;
        assertEquals(answer,value);
        int resID = res.getEventID();

        value = rrc.removeReservation(resID);
        answer = true;
        assertEquals(answer, value);

        value = rrc.removeReservation(10);
        answer = false;
        assertEquals(answer, value);

        ArrayList<Reservation> test = rrc.getReservationArrayList();
        int i;
        for(i = 0; i < test.size();i++){
            System.out.print(test.get(i).getStarTime());
            System.out.println(test.get(i).getEndTime());
        }
    }

    @Test
    public void testReservationChanges() {


        Date start = new Date(2000,11,30,14,20);
        Date end = new Date(2000,11,30,16,20);
        String testStartName = "test";

        Reservation res = new Reservation(20, testStartName, 12, start, end);
        int eventID = res.getEventID();
        RoomReservationController rrc = new RoomReservationController("testihuone", "testipaikka", false, 10, 40);

        rrc.addReservation(res);
        assertEquals(testStartName,rrc.getResByID(eventID).getEventName());

        String testName = "Perkele";
        rrc.changeEventName(eventID,testName);
        assertEquals(testName, rrc.getResByID(eventID).getEventName());

        int testReserver = 10;

        rrc.changeReserver(eventID, testReserver);
        assertEquals(testReserver,rrc.getResByID(eventID).getReserverID());

        rrc.participateToEvent(eventID, testReserver);
        rrc.participateToEvent(eventID, testReserver);

    }



}
