package com.example.aleksi.harkkat.exceptions;

/*Exception for sign in failure.*/
public class UserNotFoundException extends Exception {
    public UserNotFoundException(String message) {
        super(message);
    }
}
