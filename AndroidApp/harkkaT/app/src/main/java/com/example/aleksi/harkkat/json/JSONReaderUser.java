package com.example.aleksi.harkkat.json;

import com.example.aleksi.harkkat.exceptions.DuplicateException;
import com.example.aleksi.harkkat.reservation.Reservation;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.reservation.RoomReservationController;
import com.example.aleksi.harkkat.user.User;
import com.example.aleksi.harkkat.user.UserController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

public class JSONReaderUser {
    public static void readUserStream(InputStream is) {
        try(Reader reader = new InputStreamReader(is, "UTF-8")) {
            Gson gson = new GsonBuilder().create();
            UserController userController = UserController.getInstance();
            User[] rrcList = gson.fromJson(reader, User[].class);
            userController.clearList();
            for (User user : rrcList) {
                user.checkID();
                userController.addUser(user);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        } catch (DuplicateException e) {
            e.printStackTrace();
        }
    }
}
