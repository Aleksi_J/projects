package com.example.aleksi.harkkat.fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aleksi.harkkat.R;
import com.example.aleksi.harkkat.dialogs.SimpleDialog;
import com.example.aleksi.harkkat.json.JSONWriterReservation;
import com.example.aleksi.harkkat.reservation.Reservation;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.reservation.RoomReservationController;
import com.example.aleksi.harkkat.user.User;
import com.example.aleksi.harkkat.user.UserController;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.Calendar;


/*In this fragment user can make new reservations or delete the old ones.*/
public class ReservationFragment extends Fragment implements RecycleViewAdapt.ItemClickListener,SimpleDialog.SimpleDialogListener {
    RecyclerView rcv;
    ReservationController rc;
    Spinner spinner;
    RecycleViewAdapt rva;
    int spinnerPosition;
    int recyclerPosition;
    User user;
    DateTime pv;
    Button dateButton;
    ArrayAdapter<RoomReservationController> adapter;
    TextView capLoc;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        user = UserController.getInstance().getUserByID(getActivity().getIntent().getExtras().getInt("userID"));
        View view = inflater.inflate(R.layout.fragment_reservation, container, false);
        dateButton = (Button) view.findViewById(R.id.button2);
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateClick();
            }
        });


        Button removeButton = (Button) view.findViewById(R.id.buttonRemove);
        removeButton.setText(R.string.delete);

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeReservation();
                adapter.notifyDataSetChanged();
            }
        });
        capLoc = (TextView)view.findViewById(R.id.res_caploc);

        Button addButton = (Button) view.findViewById(R.id.buttonAdd);
        addButton.setText(R.string.add);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!user.isFrozen())
                    addNewReservation();
                else {
                    Toast.makeText(getContext(), R.string.frozen_user, Toast.LENGTH_LONG).show();
                }


            }
        });
        rcv = (RecyclerView) view.findViewById(R.id.res_rec_view);
        rc = ReservationController.getInstance();
        rcv.setLayoutManager(new LinearLayoutManager(getContext()));
        spinner = (Spinner) view.findViewById(R.id.res_roomSpinner);
        if (pv == null)
            pv = DateTime.now();
        final DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM");
        dateButton.setText(fmt.print(pv));
        this.initResSpinner(spinner);


        changeResList();
        return view;
    }

    /*Changes the reservationlist by date.*/
    public void onDateClick() {
        Calendar cal = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(year,month,dayOfMonth);
                pv = new DateTime(cal);
                final DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM");
                dateButton.setText(fmt.print(pv));
                changeResList();

            }
        },cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }


    /*Initializes roomspinner.*/
    private void initResSpinner(final Spinner spinner) {
        adapter = new ArrayAdapter<>(getContext().getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item, ReservationController.getRoomList());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerPosition = position;
                RoomReservationController item = (RoomReservationController) parent.getSelectedItem();
                changeResList();
                capLoc.setText(R.string.Location);
                capLoc.append(" " + item.getLocation()+ "\n");
                capLoc.append(getResources().getString(R.string.capacity));
                capLoc.append(" " + Integer.toString(item.getMaxCapacity()));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    public void removeReservation() {
        ReservationController rc = ReservationController.getInstance();
        rc.getRoom(adapter.getItem(spinnerPosition).getRoomID()).removeReservation(rva.getItem(recyclerPosition).getEventID(),user);
        changeResList();
        try {
            JSONWriterReservation.writeReservationStream(rc.getRoomList(),getContext());
        } catch (IOException e) {
            Toast.makeText(getContext(),R.string.write_failed, Toast.LENGTH_SHORT).show();
        }


    }

    public void changeResList() {
        rva = new RecycleViewAdapt(rc.getRoom(adapter.getItem(spinnerPosition).getRoomID()).getReservationsByTime(new DateTime(pv)), user.getUserID());
        rcv.setAdapter(rva);
        rcv.setItemAnimator(new DefaultItemAnimator());
        rva.setClickListener(this);
    }


    /*Opens dialog for adding new reservation.*/
    public void addNewReservation()  {
        SimpleDialog dialog = new SimpleDialog();
        dialog.setTargetFragment(this,0);
        dialog.show(getActivity().getSupportFragmentManager(),"lol");



    }


    /*Adding new reservations.*/
    @Override
    public void onDialogPositiveClick(DateTime start, DateTime end, int capacity, String name) {
        ReservationController rc = ReservationController.getInstance();
        try {
            DateTime startTime = new DateTime(pv.getYear(), pv.getMonthOfYear(), pv.getDayOfMonth(), start.getHourOfDay(), start.getMinuteOfHour());
            DateTime endTime = new DateTime(pv.getYear(), pv.getMonthOfYear(), pv.getDayOfMonth(), end.getHourOfDay(), end.getMinuteOfHour());
            if (!user.isFrozen()) {
                Reservation reservation = new Reservation(capacity, name, user.getUserID(), startTime.toDate(), endTime.toDate());
                if (!rc.getRoom(adapter.getItem(spinnerPosition).getRoomID()).addReservation(reservation))
                    Toast.makeText(getContext(), R.string.no_room_in_room, Toast.LENGTH_LONG);
                else {
                    Toast.makeText(getContext(), R.string.reservation_success, Toast.LENGTH_LONG).show();
                    JSONWriterReservation.writeReservationStream(ReservationController.getRoomList(), getContext());
                }
            } else {
                Toast.makeText(getContext(), R.string.frozen_user, Toast.LENGTH_LONG).show();
            }
        } catch (IndexOutOfBoundsException e) {
            Toast.makeText(getContext(), R.string.capacity_too_big, Toast.LENGTH_SHORT).show();
        } catch (IllegalAccessException e) {
            Toast.makeText(getContext(), R.string.frozen_room, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(getContext(), R.string.write_failed, Toast.LENGTH_SHORT).show();
        } catch (NullPointerException e) {
            Toast.makeText(getContext(), R.string.invalid_input, Toast.LENGTH_SHORT).show();
        }
    }


    public void onItemClick(View v, int position) {
        recyclerPosition = position;

    }
}