package com.example.aleksi.harkkat.reservation;

import com.example.aleksi.harkkat.exceptions.DuplicateException;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;

public class Reservation {
    private static ArrayList<Integer> eventIDList = new ArrayList<>();
    private int capacity;
    private String eventName;
    private int eventID;
    private int reserverID;
    private ArrayList<Integer> participants = new ArrayList<>();
    private Date starTime;
    private Date endTime;
    private static int idIterator = 0;

    public Reservation(int cap, String name, int resID, Date starTime, Date endTime) {
        capacity = cap;
        eventName = name;
        reserverID = resID;
        this.starTime = starTime;
        this.endTime = endTime;
        this.eventID = setID();
    }

    public Reservation(int cap, String name, int resID, Date starTime, Date endTime, int eventID) {
        capacity = cap;
        eventName = name;
        reserverID = resID;
        this.starTime = starTime;
        this.endTime = endTime;
        this.eventID = eventID;
        eventIDList.add(eventID);
    }

    private int setID() {
        while (eventIDList.contains(idIterator)) {
            idIterator++;
        }
        eventIDList.add(idIterator);
        return idIterator;
    }

    public void checkID() {
        if(!eventIDList.contains(this.eventID)) {
            eventIDList.add(this.eventID);
        }
    }




    public void addParticipant(int id) throws IndexOutOfBoundsException, DuplicateException {
        if (participants.size() +1 <= capacity) {
            if(!participants.contains(id)) {
                participants.add(id);
                return;

            } else {
                throw new DuplicateException("Has already this person.");
            }
        }
        throw new IndexOutOfBoundsException("No more room to this event");

    }


    public boolean checkParticipant(int id) {
        if (participants.contains(id)) {
            return true;
        }
        return false;
    }


    public boolean removeParticipant(int id) {
        if (checkParticipant(id)) {
            participants.remove(Integer.valueOf(id));
            return true;
        }
        return false;
    }

    public ArrayList<Integer> getParticipants() {
        return participants;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public int getReserverID() {
        return reserverID;
    }

    public void setReserverID(int reserverID) {
        this.reserverID = reserverID;
    }

    public Date getStarTime() {
        return starTime;
    }
    public int getUsedCapacity() {
        return participants.size();
    }

    public void setStarTime(Date starTime) {
        this.starTime = starTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getEventID() {
        return eventID;
    }
}
