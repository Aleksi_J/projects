package com.example.aleksi.harkkat.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.aleksi.harkkat.R;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.reservation.RoomReservationController;


/*Dialog for adding new room.*/
public class RoomAddDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_room_add, null);
        final EditText name = view.findViewById(R.id.roomName);
        final EditText location = view.findViewById(R.id.roomLocation);
        final EditText capasity = view.findViewById(R.id.roomCapasity);


        builder.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        RoomReservationController room = new RoomReservationController(name.getText().toString(),
                                location.getText().toString(),false,
                                Integer.parseInt(capasity.getText().toString()));
                        ReservationController rc = ReservationController.getInstance();
                        rc.addRoom(room);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RoomAddDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }

}
