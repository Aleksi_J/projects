package com.example.aleksi.harkkat.json;

import android.content.Context;


import com.example.aleksi.harkkat.user.UserController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.OutputStreamWriter;


public class JSONWriterUser {
    public static void writeUsers(String fileName, Context con) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(con.openFileOutput(fileName,Context.MODE_PRIVATE));
        Gson gson = new GsonBuilder().create();
        gson.toJson(UserController.getInstance().getUserArrayList(), writer);
        writer.close();
    }
}
