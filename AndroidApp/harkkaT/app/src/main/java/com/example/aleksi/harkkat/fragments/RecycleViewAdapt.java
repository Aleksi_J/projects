package com.example.aleksi.harkkat.fragments;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.aleksi.harkkat.R;
import com.example.aleksi.harkkat.reservation.Reservation;
import com.example.aleksi.harkkat.user.User;
import com.example.aleksi.harkkat.user.UserController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


/*Adapter for reservationList*/
public class RecycleViewAdapt extends RecyclerView.Adapter<RecycleViewAdapt.ViewHolder> {
    private ArrayList<Reservation> rooms;
    private ItemClickListener mClickListener;
    private int row_index = -1;
    private int userID;
    private User user;


    public RecycleViewAdapt(ArrayList<Reservation> rooms, int userID) {
        this.rooms = rooms;
        this.userID = userID;
        this.user = UserController.getInstance().getUserByID(userID);
    }


    @NonNull
    @Override
    public RecycleViewAdapt.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_layout, null);
        ViewHolder viewHolder =  new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Reservation res = rooms.get(i);
        SimpleDateFormat format = new SimpleDateFormat("dd.MM HH:mm");
        SimpleDateFormat format1 = new SimpleDateFormat("HH:mm");

        if(user.isSuperUser()) {
            viewHolder.txtViewTitle.setText(res.getEventName() + ", " + UserController.getInstance().getUserByID(res.getReserverID()));

        } else {
            viewHolder.txtViewTitle.setText(res.getEventName());

        }
        viewHolder.timetxt.setText("   " + format.format(res.getStarTime()) +
                "-" + format1.format(res.getEndTime()));
        viewHolder.captxt.setText(res.getUsedCapacity() + "/" + res.getCapacity());

        viewHolder.row_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = i;
                notifyDataSetChanged();
                if(mClickListener != null) mClickListener.onItemClick(v, i);
            }
        });


        /*Background-color settings.*/
        if(row_index== i) {
            viewHolder.row_linear.setBackgroundColor(Color.parseColor("#567845"));
        } else {
            if (res.getReserverID() == userID) {
                viewHolder.row_linear.setBackgroundColor(Color.parseColor("#ffd51e"));
            } else if(res.checkParticipant(userID)) {
                viewHolder.row_linear.setBackgroundColor(Color.parseColor("#a6eda6"));
            }
            else {
                viewHolder.row_linear.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        }




    }
    public int getSelectedItemID() {
        return rooms.get(row_index).getEventID();
    }

    /*Holder that has all the views of the item.*/
    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtViewTitle;
        RelativeLayout row_linear;
        private TextView timetxt;
        private TextView captxt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txtViewTitle = (TextView)itemView.findViewById(R.id.item_title);
            row_linear = (RelativeLayout) itemView.findViewById(R.id.linear_item);
            this.timetxt = (TextView)itemView.findViewById(R.id.datetxt);
            this.captxt = (TextView)itemView.findViewById(R.id.captxt);
        }

    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener{
        void onItemClick(View v, int position);
    }
    Reservation getItem(int id) {
        return rooms.get(id);
    }
}
