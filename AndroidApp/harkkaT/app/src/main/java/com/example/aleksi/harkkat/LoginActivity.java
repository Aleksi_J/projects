package com.example.aleksi.harkkat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.aleksi.harkkat.dialogs.SignInDialog;
import com.example.aleksi.harkkat.exceptions.DuplicateException;
import com.example.aleksi.harkkat.exceptions.UserNotFoundException;
import com.example.aleksi.harkkat.json.JSONReaderReservation;
import com.example.aleksi.harkkat.json.JSONReaderUser;
import com.example.aleksi.harkkat.json.JSONWriterReservation;
import com.example.aleksi.harkkat.json.JSONWriterUser;
import com.example.aleksi.harkkat.reservation.Reservation;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.reservation.RoomReservationController;
import com.example.aleksi.harkkat.user.User;
import com.example.aleksi.harkkat.user.UserController;

import org.joda.time.DateTime;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Random;

public class LoginActivity extends AppCompatActivity {

        private EditText Name;
        private EditText Password;
        UserController userController;
        private Button Login;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);

            Name = (EditText)findViewById(R.id.username);
            Password = (EditText)findViewById(R.id.passwd);
            userController = UserController.getInstance();
            Login = (Button)findViewById(R.id.button);

         //   RoomReservationController rrc = new RoomReservationController("Testi 1","testipaikka 1", false, 20);

/*
            ReservationController rc = ReservationController.getInstance();
            rc.addRoom(rrc);
            int i;
            for(i = 0; i < 10; i++) {
                Calendar cc = Calendar.getInstance();
                cc.set(2018, 11, 30, i, 20);
                Date start = new DateTime(cc.getTime()).toDate();

                cc.set(2018, 11, 30, i, 40);
                Date end = new DateTime(cc.getTime()).toDate();
                Reservation res = new Reservation(20, "turnaus", 1, start, end);
                try {
                    rc.getRoom(0).addReservation(res);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                try {
                    rc.getRoom(0).participateToEvent(i, 1);
                } catch (DuplicateException e) {
                    e.printStackTrace();
                }
            }


*/
            try {
                JSONReaderReservation.readReservationStream(LoginActivity.this.openFileInput("output.json"));
            } catch (FileNotFoundException e) {
                debugReservations();
            }


            try {
                JSONReaderUser.readUserStream(LoginActivity.this.openFileInput("users.json"));
            } catch (FileNotFoundException e) {
                debugUsers();
            }





        }
        public void checkLogin(View viewI) {
            try{
                userController = UserController.getInstance();
                int userID = userController.validate(Name.getText().toString(), Password.getText().toString());

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.putExtra("userID", userID);
                finish();
                startActivity(intent);


            } catch (UserNotFoundException e) {
                Toast.makeText(LoginActivity.this, getString(R.string.login_failed), Toast.LENGTH_LONG).show();
            }
        }

        public void signIn(View v) {
            SignInDialog dialog = new SignInDialog();
            dialog.show(getSupportFragmentManager(), "tag");
        }

/*Creates sample users for testing.*/
        private void debugUsers() {
            UserController uc = UserController.getInstance();
            try {
                uc.addUser(new User("kalle", false, false, "1"));
                uc.addUser(new User("make", false, false, "1"));
                uc.addUser(new User("superUser", false, true, "1"));
                uc.addUser(new User("jonne", false, false, "1"));

            } catch (DuplicateException e) {
                e.printStackTrace();
            }
            try {
                JSONWriterUser.writeUsers("users.json", this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
/*Creates sample reservations for testing.*/
        private void debugReservations() {
            ReservationController rc = ReservationController.getInstance();
            RoomReservationController room = new RoomReservationController("Kuntosali", "1337", false, 40);

            DateTime date = DateTime.now();

            String[] tempNames = {"Zumba", "nyrkkeily", "penkkipunnerrus", "vanhusten vuoro", "tutustumiskierros", "mm-kisat", "naisten kuntonyrkkeily",
            "miesten zumba", "keski-ikäisten vuoro", "naisten vuoro"};
            for (int i = 0; i < 10; i++) {
                Date start = new DateTime(date.getYear(), date.getMonthOfYear(),date.getDayOfMonth(), i, i*5).toDate();
                Date end = new DateTime(date.getYear(), date.getMonthOfYear(),date.getDayOfMonth(), i+1, 0).toDate();
                Reservation reservation = new Reservation(30,tempNames[i], new Random().nextInt(3)+1, start, end);
                try {
                    room.addReservation( reservation);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }


            rc.addRoom(room);
            room = new RoomReservationController("Viipuri-sali", "1222", false, 500);
            rc.addRoom(room);
            try {
                JSONWriterReservation.writeReservationStream(rc.getRoomList(), LoginActivity.this);
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
}
