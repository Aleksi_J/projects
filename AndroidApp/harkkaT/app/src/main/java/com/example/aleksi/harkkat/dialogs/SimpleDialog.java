package com.example.aleksi.harkkat.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import com.example.aleksi.harkkat.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;

/*Dialog for adding new reservation to room.*/
public class SimpleDialog extends DialogFragment {
    final DateTime pv = DateTime.now();
    final Calendar cal = Calendar.getInstance();
    private DateTime startTime;
    private DateTime endTime;
    private Button startButton;
    private Button endButton;
    private EditText etxt;
    private EditText capasity;
    SimpleDialogListener listener ;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_input, null);
        final DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");
        etxt = (EditText)view.findViewById(R.id.dialog_name);
        capasity= (EditText)view.findViewById(R.id.dialog_capacity);
        listener = (SimpleDialogListener)getTargetFragment();


        final TimePickerDialog endDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                endTime = new DateTime(pv.getYear(),pv.getMonthOfYear(),pv.getDayOfMonth(), hourOfDay, minute);
                endButton.setText(fmt.print(endTime));
            }
        },pv.getHourOfDay(),pv.getMinuteOfHour(), true);
        endDialog.setTitle(R.string.reservation_new_endtime);


        final TimePickerDialog startDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                startTime = new DateTime(pv.getYear(),pv.getMonthOfYear(),pv.getDayOfMonth(), hourOfDay, minute);
                startButton.setText(fmt.print(startTime));

            }
        },pv.getHourOfDay(),pv.getMinuteOfHour(), true);
        startDialog.setTitle(R.string.reservation_new_starttime);


        startButton = (Button) view.findViewById(R.id.button5);
        startButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startDialog.show();
            }
        });
        endButton = (Button) view.findViewById(R.id.button6);
        endButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                endDialog.show();
            }
        });


        builder.setView(view)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            listener.onDialogPositiveClick(startTime, endTime, Integer.parseInt(capasity.getText().toString()), etxt.getText().toString());
                        }catch (NumberFormatException e) {
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SimpleDialog.this.getDialog().cancel();
            }
        });
        return builder.create();
    }


    public interface SimpleDialogListener {
        public void onDialogPositiveClick(DateTime start, DateTime end, int capacity, String name);
    }


/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (SimpleDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(getActivity().toString()
                    + " must implement NoticeDialogListener");
        }
    }
*/

}
