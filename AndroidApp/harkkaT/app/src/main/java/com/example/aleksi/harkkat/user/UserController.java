package com.example.aleksi.harkkat.user;


import com.example.aleksi.harkkat.exceptions.DuplicateException;
import com.example.aleksi.harkkat.exceptions.UserNotFoundException;

import java.util.ArrayList;

public class UserController {
    private static final UserController ourInstance = new UserController();

    public static UserController getInstance() {
        return ourInstance;
    }

    public ArrayList<User> userArrayList = new ArrayList<>();


    public boolean addUser(User user) throws DuplicateException {
        for (User item : userArrayList) {
            if(item.getName().equals(user.getName())) {
                throw new DuplicateException("Username already used");
            }
        }
        userArrayList.add(user);
        return true;
    }

    public boolean removeUserByID(int userID) {
        User user;
        if ((user = getUserByID(userID)) != null) {
            userArrayList.remove(user);
            return true;
        }
        return false;
    }
    public void clearList() {
        userArrayList.clear();
    }


    public int validate(String name, String passwd) throws UserNotFoundException {
        for (User user : userArrayList) {
            if (user.validate(name, passwd) != 0) {
                return user.getUserID();
            }
        }
        throw new UserNotFoundException("No valid users with these arguments.");
    }

    public ArrayList<User> getUserArrayList() {
        return userArrayList;
    }

    public User getUserByID(int userID) {
        for (User user : userArrayList) {
            if (user.getUserID() == userID) {
                return user;
            }
        }
        return null;
    }

    public void changeName(User user, String after) throws DuplicateException {
        for (User user1: userArrayList) {
            if (user1.getName().equals(after)) {
                throw new DuplicateException("New name already used");
            }
        }
        user.setName(after);

    }


    private UserController() {
    }

}