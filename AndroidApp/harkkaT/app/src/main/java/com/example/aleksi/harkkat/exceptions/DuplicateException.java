package com.example.aleksi.harkkat.exceptions;


/*Exception for double username or reservation.*/
public class DuplicateException extends Exception {
    public DuplicateException(String message) {
        super(message);
    }
}
