This is a simple application for reserving rooms and participating to events. Please note that this application does NOT include any privacy policies.

To test this app there is couple testusers:

username: superUser
password: 1

- superUser implements user who is able to check and edit everything.

username: kalle
password: 1
- kalle implements "normal" user who can only see his own participations.
